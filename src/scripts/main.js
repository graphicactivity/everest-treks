// Init
$(document).ready(function() {
    $("#treks").hover(function() {
        $(".trekMenu").toggleClass("hidden");
        //$(".menuBg").toggleClass("dim")
    });
    $("#nepal").hover(function() {
        $(".nepalMenu").toggleClass("hidden");
        //$(".menuBg").toggleClass("dim")
    });
    // Mobile menu
    $(".mobileButton").click(function(){
        $(".mobileMenu").toggleClass("hidden");
    });
    // Landing slider
    $('.landing-slider').flickity({
        // options
        cellAlign: 'left',
        autoPlay: true,
        imagesLoaded: true,
        groupCells: false,
        draggable: false,
        contain: true,
        pageDots: true,
        prevNextButtons: false,
    });
    // Small slider
    var slider = $('.slider').flickity({
        // options
        cellAlign: 'left',
        groupCells: true,
        draggable: false,
        imagesLoaded: true,
        contain: true,
        pageDots: false,
        prevNextButtons: false,
    });
    // previous
    $('.button--prev').on( 'click', function() {
        slider.flickity('previous');
    });
    // next
    $('.button--next').on( 'click', function() {
        slider.flickity('next');
    });
    // Gallery slider
    var gallery = $('.gallery').flickity({
        // options
        cellAlign: 'left',
        groupCells: false,
        imagesLoaded: true,
        draggable: false,
        contain: true,
        pageDots: false,
        prevNextButtons: false,
    });
    // previous
    $('.button--prev').on( 'click', function() {
        gallery.flickity('previous');
    });
    // next
    $('.button--next').on( 'click', function() {
        gallery.flickity('next');
    });
    // Accordion
    $('.toggle').click(function() {

        var $this = $(this);

        // Reset close accordion icon
        $('.toggle').not($this).removeClass('spin');

        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().removeClass('spin');
            $this.removeClass('spin');
            $this.next().slideUp(350);
        } else {
            $this.addClass('spin');
            $this.parent().parent().find('li .inner').removeClass('show');
            $this.parent().parent().find('li .inner').slideUp(350);
            $this.next().toggleClass('show');
            $this.next().slideToggle(350);
        }
    });
});
