<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 0,

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        'errorTemplatePrefix' => '_',

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        'postCpLoginRedirect' => 'entries/singles',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // DO Spaces
        'accessKeyId' => getenv('ACCESS_KEY_ID'),
        'secretAccessKey' => getenv('SECRET_ACCESS_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        'useProjectConfigFile' => true,
    ],

    // Dev environment settings
    'dev' => [
        'allowAdminChanges' => true,
        'devMode' => true,
    ],

    // Staging environment settings
    'staging' => [
        'allowAdminChanges' => false,
    ],

    // Production environment settings
    'production' => [
        'allowAdminChanges' => false,
    ],
];
